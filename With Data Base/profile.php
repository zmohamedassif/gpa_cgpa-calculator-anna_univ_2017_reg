<?php
session_start();
$servername = "localhost";
$username = "students";
$password = "password";
$dbname = "gpa";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$det=$_SESSION['user_id'];

$sql = "SELECT * FROM `details` WHERE `reg no` LIKE '$det' ";
$result = mysqli_query($conn, $sql);

    // output data of each row
if (mysqli_num_rows($result) == 1) {
   $row = mysqli_fetch_assoc($result);
        $a=$row["name"];
        $b=$row["reg no"];
        $c=$row["department"];
        $d=$row["email"];
        $e=$row["college"];
    }
?>
<html>
	<head>
		<link rel="stylesheet" href="GPA-CGPA/theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>
	<body> 
		<div class="image" id="cl">

		</div>
		<div class="total-content-background">
			<div class="description" id="cont">

					<h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1>
					<h1 class="heading row" style="margin-right: 0.2%">
						<div class="col col-md-11">Anna University GPA/CGPA Calculator-2017 Regulation</div>
						<div class="col col-md-1">
							<a href="logout.php" style="background-color: lightyellow;text-decoration:none;padding:5px; color:black">Logout
							</a>
						</div>
					</h1>

					<div class="content home-content">
						<div>							
							<h1 class="content-heading">Student Profile</h1>						
							<table class="content-table">					
								<tr>
									<td>Name:</td>
									
									<td>
										<?php echo $a ?>					
									</td>
								</tr>
									
								<tr>
									<td>Register Number:</td>
									
									<td>
										<?php echo $b ?>
									</td>
								</tr>
									
								<tr>
									<td>Department:</td>
									
									<td>	
										<?php echo $c ?>
									</td>
								</tr>
									
								<tr>
									<td>E-mail:</td>
									
									<td>	
										<?php echo $d ?>
									</td>
								</tr>
									
								<tr>
									<td>College:</td>
									
									<td>	
										<?php echo $e ?>
									</td>
								</tr>
							</table>
							<a href="edit.php" target="_blank" class="links" style="margin-left:1%">Edit Profile</a><br>
							<a href="editpswd.php" class="links" style="margin-left:1%">Change Password</a>
						</div>
					</div>
			</div>
		</div>	
	</body>
</html>