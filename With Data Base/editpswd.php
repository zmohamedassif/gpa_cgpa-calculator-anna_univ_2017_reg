<html>
	<head>
		<link rel="stylesheet" href="GPA-CGPA/theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>
	<body> 
		<div class="image" id="cl">

		</div>
		<div class="total-content-background">
			<div class="description" id="cont">

				<h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1>
				<h1 class="heading"  style="text-align:center">
					<div>Anna University GPA/CGPA Calculator-2017 Regulation</div>
				</h1>
		
				<div class="content home-content">					
					<h1 class="content-heading"  style="text-align:center">Change Password</h1>
					
					<table class="content-table table-acc">	
					<tr class="tablemod">
						<td class="tablemod">Old Password:</td>
						<td class="tablemod"><input type="password" name="password" id="opswd" class="inputmod"></td>						
					</tr>						
					<tr class="tablemod">
						<td class="tablemod">New Password:</td>
						<td class="tablemod"><input type="password" name="password" id="npswd"  class="inputmod"></td>
                    </tr>
                    <tr class="tablemod">
						<td class="tablemod">Retype New Password:</td>
						<td class="tablemod"><input type="password" name="password" id="rpswd"  class="inputmod"></td>
					</tr>
									
					</table>
					<div class="submitalign">														
						<input class="submitmod login-button" type="submit" id="SUBMIT">				
					</div>

				</div>
			</div>				
		</div>
	</body>
	<script type="text/javascript" src="editpswd.js"></script>
</html>
