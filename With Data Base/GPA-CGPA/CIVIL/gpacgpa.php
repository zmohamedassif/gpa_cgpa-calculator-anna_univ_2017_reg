<?php
include '../../Include Files/civil.php';
?>
<html>
	<head>
		<link rel="stylesheet" href="../theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	</head>
	<body> 
	<div class="image" id="cl">

	</div>
<div class="total-content-background">
	<div class="description" id="cont">
		

			<h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1>
			<h1 class="heading row" style="margin-right: 0.2%">
				<div class="col col-md-11">Anna University GPA/CGPA Calculator-2017 Regulation</div>
				<div class="col col-md-1">
					<a href="../../logout.php" style="background-color: lightyellow;text-decoration:none;padding:5px; color:black">Logout
					</a>
				</div>
			</h1>
	
			<div class="content home-content">
				<div class="home-content-whole">
					<h1 class="dept" style="text-align: center">Civil Engineering</h1>
					<h1>User Name: <?php echo $_SESSION['user_name']; ?> </h1>
					<a href="../../profile.php" >Profile</a>						
					<a href="Semester-1.php">
					<h1 class="home-link" style="margin-top:50px">
						<i class="fa fa-calculator" aria-hidden="true"></i>  Grade Point Average(GPA)
					</h1></a>
					<a href="../CGPA/CIVIL/Semester-1.php">
					<h1 class="home-link">
						<i class="fa fa-calculator" aria-hidden="true"></i>  Cumulative Grade Point Average(CGPA)
					</h1></a>
				</div>
			</div>
	</div>
</div>

			
	</body>
</html>