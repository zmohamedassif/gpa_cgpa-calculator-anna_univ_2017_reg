<?php 
	include '../../../Include Files/mech.php';
?>
<html>
	<head>
		<link rel="stylesheet" href="../../theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="../../font-awesome-4.7.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

	</head>
	<body> 
	<div class="image" id="cl">

	</div>
<div class="total-content-background">
	<div class="description" id="cont">

			<h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1><h1 class="heading row" style="margin-right: 0.2%">
				
				<div class="col col-md-11"><button onclick="dispmod()" class="calc-button menu"><i class="fa fa-bars" aria-hidden="true"></i></button> Anna University GPA/CGPA Calculator-2017 Regulation</div><div class="col col-md-1"><a href="../../../logout.php" style="background-color: lightyellow;text-decoration:none;padding:5px; color:black;margin-top:1%">Logout</a></div>
			</h1>
	
			<ul class="sem-list group" id="nav">
				<li><button onclick="closeitmod()"><i class="fa fa-times" aria-hidden="true"></i> close</button></li>
				<li><a href="../../MECH/gpacgpa.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
				<li><a href="Semester-1.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-1</a></li>
				<li><a href="Semester-2.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-2</a></li>
				<li><a href="Semester-3.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-3</a></li>
				<li><a href="Semester-4.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-4</a></li>
				<li><a href="Semester-5.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-5</a></li>
				<li><a href="Semester-6.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-6</a></li>
				<li><a href="Semester-7.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-7</a></li>
				<li><a href="Semester-8.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-8</a></li>
			</ul>
	
			<div class="content"><h1 class="content-heading">Mechanical Engineering [ User Name: <?php echo $_SESSION['user_name']; ?> ]</h1>
				<h1 class="content-heading">Seven Semesters</h1>

			<!--grid used-->
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-1]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Communicative English<span class="code">[HS8151]</span></td>
						<td class="code-lap">HS8151</td>
						<td>
							<select id="1"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Mathematics - I<span class="code">[MA8151]</span></td>
						<td class="code-lap">MA8151</td>
						<td>
							<select id="2"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Physics<span class="code">[PH8151]</span></td>
						<td class="code-lap">PH8151</td>
						<td>	
							<select id="3"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Chemistry<span class="code">[CY8151]</span></td>
						<td class="code-lap">CY8151</td>
						<td>	
							<select id="4"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Problem Solving and Python Programming<span class="code">[GE8151]</span></td>
						<td class="code-lap">GE8151</td>
						<td>	
							<select id="5"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Engineering Graphics<span class="code">[GE8152]</span></td>
						<td class="code-lap">GE8152</td>
						<td>
							<select id="6"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Problem Solving and Python Programming Laboratory<span><span class="code">[GE8161]</span></td>
						<td class="code-lap">GE8161</td>
						<td>
							<select id="7"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Physics and Chemistry Laboratory<span class="code">[BS8161]</span></td>
							<td class="code-lap">BS8161</td>
						<td>	
							<select id="8"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-2]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Technical English </span><span class="code">[HS8251]</span></td>
						<td class="code-lap">HS8251</td>
						<td>
							<select id="9"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Mathematics - II</span><span class="code">[MA8251]</span></td>
						<td class="code-lap">MA8251</td>
						<td>
							<select id="10"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Materials Science</span><span class="code">[PH8251]</span></td>
						<td class="code-lap">PH8251</td>
						<td>	
							<select id="11"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Basic Electrical, Electronics and Instrumentation Engineering</span><span class="code">[BE8253]</span></td>
						<td class="code-lap">BE8253</td>
						<td>	
							<select id="12"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Environmental Science and Engineering</span><span class="code">[GE8291]</span></td>
						<td class="code-lap">GE8291</td>
						<td>	
							<select id="13"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Engineering Mechanics</span><span class="code">[GE8292]</span></td>
						<td class="code-lap">GE8292</td>
						<td>
							<select id="14"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Practices Laboratory</span><span class="code">[GE8261]</span></td>
						<td class="code-lap"></td>
						<td>
							<select id="15"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>GE8261
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Basic Electrical, Electronics and Instrumentation Engineering Laboratory</span><span class="code">[BE8261]</span></td>
						<td class="code-lap">BE8261</td>
						<td>	
							<select id="16"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
			</div>
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-3]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Transforms and Partial Differential Equations</span><span class="code">[MA8353]</span></td>
						<td class="code-lap">MA8353</td>
						<td>
							<select id="17"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Thermodynamics</span><span class="code">[ME8391]</span></td>
						<td class="code-lap">ME8391</td>
						<td>
							<select id="18"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Fluid Mechanics and Machinery E</span><span class="code">[CE8394]</span></td>
						<td class="code-lap">CE8394</td>
						<td>	
							<select id="19"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Manufacturing Technology - I</span><span class="code">[ME8351]</span></td>
						<td class="code-lap">ME8351</td>
						<td>	
							<select id="20"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Electrical Drives and Controls</span><span class="code">[EE8353]</span></td>
						<td class="code-lap">EE8353</td>
						<td>	
							<select id="21"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Manufacturing Technology Laboratory - I</span><span class="code">[ME8361]</span></td>
						<td class="code-lap">ME8361</td>
						<td>
							<select id="22"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Computer Aided Machine Drawing</span><span class="code">[ME8381]</span></td>
						<td class="code-lap">ME8381</td>
						<td>
							<select id="23"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Electrical Engineering Laboratory</span><span class="code">[EE8361]</span></td>
						<td class="code-lap">EE8361</td>
						<td>	
							<select id="24"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Interpersonal Skills / Listening & Speaking</span><span class="code">[HS8381]</span></td>
						<td class="code-lap">HS8381</td>
						<td>
							<select id="25"  credit="1">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-4]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Statistics and Numerical Methods</span><span class="code">[MA8452]</span></td>
						<td class="code-lap">MA8452</td>
						<td>
							<select id="26"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Kinematics of Machinery</span><span class="code">[ME8492]</span></td>
						<td class="code-lap">ME8492</td>
						<td>
							<select id="27"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Manufacturing Technology – II</span><span class="code">[ME8451]</span></td>
						<td class="code-lap">ME8451</td>
						<td>	
							<select id="28"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Metallurgy</span><span class="code">[ME8491]</span></td>
						<td class="code-lap">ME8491</td>
						<td>	
							<select id="29"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Strength of Materials for Mechanical Engineers</span><span class="code">[CE8395]</span></td>
						<td class="code-lap">CE8395</td>
						<td>	
							<select id="30"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Thermal Engineering- I</span><span class="code">[ME8493]</span></td>
						<td class="code-lap">ME8493</td>
						<td>
							<select id="31"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Manufacturing Technology Laboratory – II</span><span class="code">[ME8462]</span></td>
						<td class="code-lap">ME8462</td>
						<td>
							<select id="32"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Strength of Materials and Fluid Mechanics and Machinery Laboratory</span><span class="code">[CE8381]</span></td>
						<td class="code-lap">CE8381</td>
						<td>	
							<select id="33"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Advanced Reading and Writing</span><span class="code">[HS8461]</span></td>
						<td class="code-lap">HS8461</td>
						<td>
							<select id="34"  credit="1">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
			</div>
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-5]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Thermal Engineering- II</span><span class="code">[ME8595]</span></td>
						<td class="code-lap">ME8595</td>
						<td>
							<select id="35"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Design of Machine Elements</span><span class="code">[ME8593]</span></td>
						<td class="code-lap">ME8593</td>
						<td>
							<select id="36"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Metrology and Measurements<span class="code">[ME8501]</span></td>
						<td class="code-lap">ME8501</td>
						<td>	
							<select id="37"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Dynamics of Machines <span class="code">[ME8594]</span></td>
						<td class="code-lap">ME8594</td>
						<td>	
							<select id="38"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Open Elective I<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>	
							<select id="39"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Kinematics and Dynamics Laboratory<span class="code">[ME8511]</span></td>
						<td class="code-lap">ME8511</td>
						<td>
							<select id="40"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Thermal Engineering Laboratory<span class="code">[ME8512]</span></td>
						<td class="code-lap">ME8512</td>
						<td>
							<select id="41"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Metrology and Measurements Laboratory<span class="code">[ME8513]</span></td>
						<td class="code-lap">ME8513</td>
						<td>	
							<select id="42"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-6]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Design of Transmission Systems<span class="code">[ME8651]</span></td>
						<td class="code-lap">ME8651</td>
						<td>
							<select id="43"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Computer Aided Design and Manufacturing<span class="code">[ME8691]</span></td>
						<td class="code-lap">ME8691</td>
						<td>
							<select id="44"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Heat and Mass Transfer<span class="code">[ME8693]</span></td>
						<td class="code-lap">ME8693</td>
						<td>	
							<select id="45"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Finite Element Analysis<span class="code">[ME8692]</span></td>
						<td class="code-lap">ME8692</td>
						<td>	
							<select id="46"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Hydraulics and Pneumatics<span class="code">[ME8694]</span></td>
						<td class="code-lap">ME8694</td>
						<td>	
							<select id="47"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Professional Elective - I<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>
							<select id="48"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>CAD / CAM Laboratory<span class="code">[ME8681]</span></td>
						<td class="code-lap">ME8681</td>
						<td>
							<select id="49"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Design and Fabrication Project<span class="code">[ME8682]</span></td>
						<td class="code-lap">ME8682</td>
						<td>	
							<select id="50"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Professional Communication<span class="code">[HS8581]</span></td>
						<td class="code-lap">HS8581</td>
						<td>
							<select id="51"  credit="1">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
					
						
					</table>
					
				</div>
			</div>
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-7]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Power Plant Engineering<span class="code">[ME8792]</span></td>
						<td class="code-lap">ME8792</td>
						<td>
							<select id="52"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Process Planning and Cost Estimation<span class="code">[ME8793]</span></td>
						<td class="code-lap">ME8793</td>
						<td>
							<select id="53"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Mechatronics<span class="code">[ME8791]</span></td>
						<td class="code-lap">ME8791</td>
						<td>	
							<select id="54"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Open Elective - II<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>	
							<select id="55"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Professional Elective – II<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>	
							<select id="56"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
					
					<tr>
						<td>Professional Elective – III<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>
							<select id="57"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>

					<tr>
						<td>Simulation and Analysis Laboratory<span class="code">[ME8711]</span></td>
						<td class="code-lap">ME8711</td>
						<td>
							<select id="58"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
										
					<tr>
						<td>Mechatronics Laboratory<span class="code">[ME8781]</span></td>
						<td class="code-lap">ME8781</td>
						<td>	
							<select id="59"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Technical Seminar<span class="code">[ME8712]</span></td>
						<td class="code-lap">ME8712</td>
						<td>
							<select id="60"  credit="1">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<div class="result group">
						<p>Cumulative Grade Point Average(CGPA):</p>
						<div id="ans"></div>
						<P>Percentage:</P>
						<div class="percentage" id="perc-resp">
							<div class="percentage-amount" id="percentage"></div>
						</div>
						<P><button id="singlejs" class="calc-button calc-size">Calculate</button>
						<P><button id="save" class="calc-button calc-size">Save</button>
					</div>
				</div>
			</div>
			<!--grid used-->

		</div>



	</div>
</div>


			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script><script type="text/javascript" src="../../jquery.js"></script><script type="text/javascript" src="../../theme.js"></script><script src="../thememod.js"></script>

			<script type="text/javascript">
		$(document).ready(function()
		{
			var i;
			var sub_no=1;
			var mark=<?php echo json_encode($mark); ?>;
			$("select").each(function()
			{
				i=this.id;				
				var a1=$('#'+i).val(mark[sub_no]);
				sub_no++;
			});
	  		var reg1=<?php echo $reg; ?>;
	  		$("#save").click(function()
  			{
				sub_no=1;
				$("select").each(function()
				{
					i=this.id;	
					mark[sub_no]=$('#'+i).val();								
					sub_no++;
				});
  				
		  		 $.ajax({
	                type: "POST",
	                url: '../../../Update Files/mech.php',
	                data : {
	                	reg2:reg1,
                     	mark2:mark,           
	                },
	                success: function(data)
	                {
	                 if(data==1)
	                 {

	                    alert("Marks Updated Successful");	                   
	                 }  
	                 else
	                 {
	                    alert("Something is Wrong Please Contact admin");
	                 }                    
	                }
	            });	

	        });

		});				
	</script>
			
	</body>
</html>