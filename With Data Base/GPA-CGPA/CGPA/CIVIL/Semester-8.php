<?php 
	include '../../../Include Files/civil.php';
?>
<html>
	<head>
		<link rel="stylesheet" href="../../theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="../../font-awesome-4.7.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

	</head>
	<body> 
	<div class="image" id="cl">

	</div>
<div class="total-content-background">
	<div class="description" id="cont">

			<h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1><h1 class="heading row" style="margin-right: 0.2%">
				
				<div class="col col-md-11"><button onclick="dispmod()" class="calc-button menu"><i class="fa fa-bars" aria-hidden="true"></i></button> Anna University GPA/CGPA Calculator-2017 Regulation</div><div class="col col-md-1"><a href="../../../logout.php" style="background-color: lightyellow;text-decoration:none;padding:5px; color:black;margin-top:1%">Logout</a></div>
			</h1>
	
			<ul class="sem-list group" id="nav">
				<li><button onclick="closeitmod()"><i class="fa fa-times" aria-hidden="true"></i> close</button></li>
				<li><a href="../../CIVIL/gpacgpa.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
				<li><a href="Semester-1.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-1</a></li>
				<li><a href="Semester-2.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-2</a></li>
				<li><a href="Semester-3.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-3</a></li>
				<li><a href="Semester-4.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-4</a></li>
				<li><a href="Semester-5.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-5</a></li>
				<li><a href="Semester-6.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-6</a></li>
				<li><a href="Semester-7.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-7</a></li>
				<li><a href="Semester-8.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-8</a></li>
			</ul>
	
			<div class="content"><h1 class="content-heading">Civil Engineering [ User Name: <?php echo $_SESSION['user_name']; ?> ]</h1>
				<h1 class="content-heading">Eight Semesters</h1>

			<!--grid used-->
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-1]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Communicative English<span class="code">[HS8151]</span></td>
						<td class="code-lap">HS8151</td>
						<td>
							<select id="1"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Mathematics - I<span class="code">[MA8151]</span></td>
						<td class="code-lap">MA8151</td>
						<td>
							<select id="2"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Physics<span class="code">[PH8151]</span></td>
						<td class="code-lap">PH8151</td>
						<td>	
							<select id="3"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Chemistry<span class="code">[CY8151]</span></td>
						<td class="code-lap">CY8151</td>
						<td>	
							<select id="4"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Problem Solving and Python Programming<span class="code">[GE8151]</span></td>
						<td class="code-lap">GE8151</td>
						<td>	
							<select id="5"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Engineering Graphics<span class="code">[GE8152]</span></td>
						<td class="code-lap">GE8152</td>
						<td>
							<select id="6"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Problem Solving and Python Programming Laboratory<span><span class="code">[GE8161]</span></td>
						<td class="code-lap">GE8161</td>
						<td>
							<select id="7"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Physics and Chemistry Laboratory<span class="code">[BS8161]</span></td>
							<td class="code-lap">BS8161</td>
						<td>	
							<select id="8"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-2]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Technical English<span class="code">[HS8251]</span></td>
						<td class="code-lap">HS8251</td>
						<td>
							<select id="9"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Mathematics – II<span class="code">[MA8251]</span></td>
						<td class="code-lap">MA8251</td>
						<td>
							<select id="10"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Physics For Civil Engineering <span class="code">[PH8201]</span></td>
						<td class="code-lap">PH8201</td>
						<td>	
							<select id="11"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Basic Electrical and Electronics Engineering<span class="code">[BE8251]</span></td>
						<td class="code-lap">BE8251</td>
						<td>	
							<select id="12"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Environmental Science and Engineering<span class="code">[GE8291]</span></td>
						<td class="code-lap">GE8291</td>
						<td>	
							<select id="13"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Engineering Mechanics<span class="code">[GE8292]</span></td>
						<td class="code-lap">GE8292</td>
						<td>
							<select id="14"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Practices Laboratory<span class="code">[GE8261]</span></td>
						<td class="code-lap">GE8261</td>
						<td>
							<select id="15"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Computer Aided Building Drawing<span class="code">[CE8211]</span></td>
						<td class="code-lap">CE8211</td>
						<td>	
							<select id="16"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
			</div>
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-3]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Transforms and Partial Differential Equations<span class="code">[MA8353]</span></td>
						<td class="code-lap">MA8353</td>
						<td>
							<select id="17"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Strength of Materials I<span class="code">[CE8301]</span></td>
						<td class="code-lap">CE8301</td>
						<td>
							<select id="18"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Fluid Mechanics<span class="code">[CE8302]</span></td>
						<td class="code-lap">CE8302</td>
						<td>	
							<select id="19"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Surveying<span class="code">[CE8351]</span></td>
						<td class="code-lap">CE8351</td>
						<td>	
							<select id="20"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Construction Materials<span class="code">[CE8391]</span></td>
						<td class="code-lap">CE8391</td>
						<td>	
							<select id="21"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Engineering Geology<span class="code">[CE8392]</span></td>
						<td class="code-lap">CE8392</td>
						<td>
							<select id="22"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Construction Materials Laboratory<span class="code">[CE8311]</span></td>
						<td class="code-lap">CE8311</td>
						<td>
							<select id="23"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Surveying Laboratory<span class="code">[CE8361]</span></td>
						<td class="code-lap">CE8361</td>
						<td>	
							<select id="24"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td> Interpersonal Skills / Listening and Speaking<span class="code">[HS8381]</span></td>
						<td class="code-lap">HS8381</td>
						<td>
							<select id="25"  credit="1">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
					
				</div>
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-4]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Numerical Methods<span class="code">[MA8491]</span></td>
						<td class="code-lap">MA8491</td>
						<td>
							<select id="26"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Construction Techniques and Practices<span class="code">[CE8401]</span></td>
						<td class="code-lap">CE8401</td>
						<td>
							<select id="27"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Strength of Materials II<span class="code">[CE8402]</span></td>
						<td class="code-lap">CE8402</td>
						<td>	
							<select id="28"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Applied Hydraulic Engineering<span class="code">[CE8403]</span></td>
						<td class="code-lap">CE8403</td>
						<td>	
							<select id="29"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Concrete Technology<span class="code">[CE8404]</span></td>
						<td class="code-lap">CE8404</td>
						<td>	
							<select id="30"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Soil CIVILanics<span class="code">[CE8491]</span></td>
						<td class="code-lap">CE8491</td>
						<td>
							<select id="31"  credit="3">Soil CIVILanics 
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Strength of MaterialsLaboratory<span class="code">[CE8481]</span></td>
						<td class="code-lap">CE8481</td>
						<td>
							<select id="32"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Hydraulic Engineering Laboratory<span class="code">[CE8461]</span></td>
						<td class="code-lap">CE8461</td>
						<td>	
							<select id="33"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Advanced Reading and Writing<span class="code">[HS8461]</span></td>
						<td class="code-lap">HS8461</td>
						<td>
							<select id="34"  credit="1">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
			</div>
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-5]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Design of Reinforced Cement Concrete Elements<span class="code">[CE8501]</span></td>
						<td class="code-lap">CE8501</td>
						<td>
							<select id="35"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Structural Analysis I<span class="code">[CE8502]</span></td>
						<td class="code-lap">CE8502</td>
						<td>
							<select id="36"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Water Supply Engineering<span class="code">[EN8491]</span></td>
						<td class="code-lap">EN8491</td>
						<td>	
							<select id="37"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Foundation Engineering<span class="code">[CE8591]</span></td>
						<td class="code-lap">CE8591</td>
						<td>	
							<select id="38"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Professional Elective I<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>	
							<select id="39"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Open Elective I*<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>
							<select id="40"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Soil Mechanics Laboratory<span class="code">[CE8511]</span></td>
						<td class="code-lap">CE8511</td>
						<td>
							<select id="41"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Water and Waste Water Analysis Laboratory<span class="code">[CE8512]</span></td>
						<td class="code-lap">CE8512</td>
						<td>	
							<select id="42"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Survey Camp (2 weeks â€“During IV Semester)<span class="code">[CE8513]</span></td>
						<td class="code-lap">CE8513</td>
						<td>
							<select id="43"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-6]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Design of Steel Structural Elements<span class="code">[CE8601]</span></td>
						<td class="code-lap">CE8601</td>
						<td>
							<select id="44"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Structural Analysis II<span class="code">[CE8602]</span></td>
						<td class="code-lap">CE8602</td>
						<td>
							<select id="45"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Irrigation Engineering<span class="code">[CE8603]</span></td>
						<td class="code-lap">CE8603</td>
						<td>	
							<select id="46"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Highway Engineering<span class="code">[CE8604]</span></td>
						<td class="code-lap">CE8604</td>
						<td>	
							<select id="47"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Wastewater Engineering<span class="code">[EN8592]</span></td>
						<td class="code-lap">EN8592</td>
						<td>	
							<select id="48"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Professional Elective II<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>
							<select id="49"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Highway Engineering Laboratory<span class="code">[CE8611]</span></td>
						<td class="code-lap">CE8611</td>
						<td>
							<select id="50"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Irrigation and Environmental Engineering Drawing<span class="code">[CE8612]</span></td>
						<td class="code-lap">CE8612</td>
						<td>	
							<select id="51"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Professional Communication<span class="code">[HS8581]</span></td>
						<td class="code-lap">HS8581</td>
						<td>
							<select id="52"  credit="1">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
			</div>
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-7]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Estimation,
Costing and
Valuation
Engineering<span class="code">[CE8701]</span></td>
						<td class="code-lap">CE8701</td>
						<td>
							<select id="53"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Railways, Airports, Docks and Harbour Engineering<span class="code">[CE8702]</span></td>
						<td class="code-lap">CE8702</td>
						<td>
							<select id="54"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Structural Design and Drawing<span class="code">[CE8703]</span></td>
						<td class="code-lap">CE8703</td>
						<td>	
							<select id="55"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Professional Elective III<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>	
							<select id="56"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Open Elective II*<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>	
							<select id="57"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Creative and Innovative Project (Activity Based - Subject [Semester-1] Related)<span class="code">[CE8711]</span></td>
						<td class="code-lap">CE8711</td>
						<td>
							<select id="58"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Industrial Training (4 weeks During VI Semester – Summer)<span class="code">[CE8712]</span></td>
						<td class="code-lap">CE8712</td>
						<td>
							<select id="59"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-8]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Professional Elective IV<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>
							<select id="60"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Professional Elective V<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>
							<select id="61"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Project Work<span class="code">[CE8811]</span></td>
						<td class="code-lap">CE8811</td>
						<td>	
							<select id="62"  credit="10">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>

			</div>
			<!--grid used-->
			<div class="row">
				<div class="col col-md-6">
					<div class="result group fix">
						<p>Cumulative Grade Point Average(CGPA):</p>
							<div id="ans"></div>
						<P>Percentage:</P>
							<div class="percentage" id="perc-resp">
								<div class="percentage-amount" id="percentage"></div>
							</div>
						<P><button id="singlejs" class="calc-button calc-size">Calculate</button>
						<P><button id="save" class="calc-button calc-size">Save</button>
					</div>
				</div>
			</div>

		</div>



	</div>
</div>


			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script><script type="text/javascript" src="../../jquery.js"></script><script type="text/javascript" src="../../theme.js"></script><script src="../thememod.js"></script>

			<script type="text/javascript">
		$(document).ready(function()
		{
			var i;
			var sub_no=1;
			var mark=<?php echo json_encode($mark); ?>;
			$("select").each(function()
			{
				i=this.id;				
				var a1=$('#'+i).val(mark[sub_no]);
				sub_no++;
			});
	  		var reg1=<?php echo $reg; ?>;
	  		$("#save").click(function()
  			{
				sub_no=1;
				$("select").each(function()
				{
					i=this.id;	
					mark[sub_no]=$('#'+i).val();								
					sub_no++;
				});
  				
		  		 $.ajax({
	                type: "POST",
	                url: '../../../Update Files/civil.php',
	                data : {
	                	reg2:reg1,
                     	mark2:mark,           
	                },
	                success: function(data)
	                {
	                 if(data==1)
	                 {

	                    alert("Marks Updated Successful");	                   
	                 }  
	                 else
	                 {
	                    alert("Something is Wrong Please Contact admin");
	                 }                    
	                }
	            });	

	        });

		});				
	</script>
			
	</body>
</html>