<?php 
	include '../../../Include Files/cse.php';
?>

<html>
	<head>
		<link rel="stylesheet" href="../../theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="../../font-awesome-4.7.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

	</head>
	<body> 
	<div class="image" id="cl">

	</div>
<div class="total-content-background">
	<div class="description" id="cont">

			<h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1><h1 class="heading row" style="margin-right: 0.2%">
				
				<div class="col col-md-11"><button onclick="dispmod()" class="calc-button menu"><i class="fa fa-bars" aria-hidden="true"></i></button> Anna University GPA/CGPA Calculator-2017 Regulation</div><div class="col col-md-1"><a href="../../../logout.php" style="background-color: lightyellow;text-decoration:none;padding:5px; color:black;margin-top:1%">Logout</a></div>
			</h1>
	
			<ul class="sem-list group" id="nav">
				<li><button onclick="closeitmod()"><i class="fa fa-times" aria-hidden="true"></i> close</button></li>
				<li><a href="../../CSE/gpacgpa.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
				<li><a href="Semester-1.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-1</a></li>
				<li><a href="Semester-2.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-2</a></li>
				<li><a href="Semester-3.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-3</a></li>
				<li><a href="Semester-4.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-4</a></li>
				<li><a href="Semester-5.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-5</a></li>
				<li><a href="Semester-6.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-6</a></li>
				<li><a href="Semester-7.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-7</a></li>
				<li><a href="Semester-8.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-8</a></li>
			</ul>
	
			<div class="content"><h1 class="content-heading">Computer Science and Engineering [ User Name: <?php echo $_SESSION['user_name']; ?> ]</h1>
				<h1 class="content-heading">One Semester</h1>

			<!--grid used-->
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-1]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Communicative English<span class="code">[HS8151]</span></td>
						<td class="code-lap">HS8151</td>
						<td>
							<select id="1"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Mathematics - I<span class="code">[MA8151]</span></td>
						<td class="code-lap">MA8151</td>
						<td>
							<select id="2"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Physics<span class="code">[PH8151]</span></td>
						<td class="code-lap">PH8151</td>
						<td>	
							<select id="3"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Chemistry<span class="code">[CY8151]</span></td>
						<td class="code-lap">CY8151</td>
						<td>	
							<select id="4"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Problem Solving and Python Programming<span class="code">[GE8151]</span></td>
						<td class="code-lap">GE8151</td>
						<td>	
							<select id="5"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Engineering Graphics<span class="code">[GE8152]</span></td>
						<td class="code-lap">GE8152</td>
						<td>
							<select id="6"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Problem Solving and Python Programming Laboratory<span><span class="code">[GE8161]</span></td>
						<td class="code-lap">GE8161</td>
						<td>
							<select id="7"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Physics and Chemistry Laboratory<span class="code">[BS8161]</span></td>
							<td class="code-lap">BS8161</td>
						<td>	
							<select id="8"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<div class="result group">
						<p>Cumulative Grade Point Average(CGPA):</p>
						<div id="ans"></div>
						<P>Percentage:</P>
						<div class="percentage" id="perc-resp">
							<div class="percentage-amount" id="percentage"></div>
						</div>
						<P><button id="singlejs" class="calc-button calc-size">Calculate</button>
						<P><button id="save" class="calc-button calc-size">Save</button>
					</div>
				</div>
			</div>
			<!--grid used-->

		</div>



	</div>
</div>


			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script><script type="text/javascript" src="../../jquery.js"></script><script type="text/javascript" src="../../theme.js"></script><script src="../thememod.js"></script>

			<script type="text/javascript">
		$(document).ready(function()
		{
			var i;
			var sub_no=1;
			var mark=<?php echo json_encode($mark); ?>;
			$("select").each(function()
			{
				i=this.id;				
				var a1=$('#'+i).val(mark[sub_no]);
				sub_no++;
			});
	  		var reg1=<?php echo $reg; ?>;
	  		$("#save").click(function()
  			{
				sub_no=1;
				$("select").each(function()
				{
					i=this.id;	
					mark[sub_no]=$('#'+i).val();								
					sub_no++;
				});
  				
		  		 $.ajax({
	                type: "POST",
	                url: '../../../Update Files/cse.php',
	                data : {
	                	reg2:reg1,
                     	mark2:mark,           
	                },
	                success: function(data)
	                {
	                 if(data==1)
	                 {

	                    alert("Marks Updated Successful");	                   
	                 }  
	                 else
	                 {
	                    alert("Something is Wrong Please Contact admin");
	                 }                    
	                }
	            });	

	        });

		});				
	</script>
			
			
	</body>
</html>