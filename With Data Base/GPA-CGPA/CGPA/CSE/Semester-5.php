<?php 
	include '../../../Include Files/cse.php';
?>

<html>
	<head>
		<link rel="stylesheet" href="../../theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="../../font-awesome-4.7.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

	</head>
	<body> 
	<div class="image" id="cl">

	</div>
<div class="total-content-background">
	<div class="description" id="cont">

			<h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1><h1 class="heading row" style="margin-right: 0.2%">
				
				<div class="col col-md-11"><button onclick="dispmod()" class="calc-button menu"><i class="fa fa-bars" aria-hidden="true"></i></button> Anna University GPA/CGPA Calculator-2017 Regulation</div><div class="col col-md-1"><a href="../../../logout.php" style="background-color: lightyellow;text-decoration:none;padding:5px; color:black;margin-top:1%">Logout</a></div>
			</h1>
	
			<ul class="sem-list group" id="nav">
				<li><button onclick="closeitmod()"><i class="fa fa-times" aria-hidden="true"></i> close</button></li>
				<li><a href="../../CSE/gpacgpa.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
				<li><a href="Semester-1.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-1</a></li>
				<li><a href="Semester-2.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-2</a></li>
				<li><a href="Semester-3.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-3</a></li>
				<li><a href="Semester-4.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-4</a></li>
				<li><a href="Semester-5.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-5</a></li>
				<li><a href="Semester-6.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-6</a></li>
				<li><a href="Semester-7.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-7</a></li>
				<li><a href="Semester-8.php"><i class="fa fa-book" aria-hidden="true"></i> Semester-8</a></li>
			</ul>
	
			<div class="content"><h1 class="content-heading">Computer Science and Engineering [ User Name: <?php echo $_SESSION['user_name']; ?> ]</h1>
				<h1 class="content-heading">Five Semesters</h1>

			<!--grid used-->
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-1]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Communicative English<span class="code">[HS8151]</span></td>
						<td class="code-lap">HS8151</td>
						<td>
							<select id="1"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Mathematics - I<span class="code">[MA8151]</span></td>
						<td class="code-lap">MA8151</td>
						<td>
							<select id="2"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Physics<span class="code">[PH8151]</span></td>
						<td class="code-lap">PH8151</td>
						<td>	
							<select id="3"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Chemistry<span class="code">[CY8151]</span></td>
						<td class="code-lap">CY8151</td>
						<td>	
							<select id="4"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Problem Solving and Python Programming<span class="code">[GE8151]</span></td>
						<td class="code-lap">GE8151</td>
						<td>	
							<select id="5"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Engineering Graphics<span class="code">[GE8152]</span></td>
						<td class="code-lap">GE8152</td>
						<td>
							<select id="6"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Problem Solving and Python Programming Laboratory<span><span class="code">[GE8161]</span></td>
						<td class="code-lap">GE8161</td>
						<td>
							<select id="7"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Physics and Chemistry Laboratory<span class="code">[BS8161]</span></td>
							<td class="code-lap">BS8161</td>
						<td>	
							<select id="8"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-2]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Technical English<span class="code">[HS8251]</span></td>
						<td class="code-lap">HS8251</td>
						<td>
							<select id="9"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Mathematics - II<span class="code">[MA8251]</span></td>
						<td class="code-lap">MA825</td>
						<td>
							<select id="10"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Physics for Information Science<span class="code">[PH8252]</span></td>
						<td class="code-lap">PH8252</td>
						<td>	
							<select id="11"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Basic Electrical,Electronics and Measurement Engineering <span class="code">[BE8255]</span></td>
						<td class="code-lap">BE8255</td>
						<td>	
							<select id="12"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Environmental Science and Engineering<span class="code">[GE8291]</span></td>
						<td class="code-lap">GE8291</td>
						<td>	
							<select id="13"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Programming in C<span class="code">[CS8251]</span></td>
						<td class="code-lap">CS8251</td>
						<td>
							<select id="14"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Engineering Practices Laboratory<span class="code">[GE8261]</span></td>
						<td class="code-lap">GE8261</td>
						<td>
							<select id="15"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>C Programming Laboratory<span class="code">[CS8261]</span></td>
						<td class="code-lap">CS8261</td>
						<td>	
							<select id="16"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
			</div>
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-3]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Discrete Mathematics<span class="code">[MA8351]</span></td>
						<td class="code-lap">MA8351</td>
						<td>
							<select id="17"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Digital Principles and System Design<span class="code">[CS8351]</span></td>
						<td class="code-lap">CS8351</td>
						<td>
							<select id="18"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Data Structures<span class="code">[CS8391]</span></td>
						<td class="code-lap">CS8391</td>
						<td>	
							<select id="19"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Object Oriented Programming<span class="code">[CS8392]</span></td>
						<td class="code-lap">CS8392</td>
						<td>	
							<select id="20"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Communication Engineering <span class="code">[EC8395]</span></td>
						<td class="code-lap">EC8395</td>
						<td>	
							<select id="21"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Data Structures Laboratory<span class="code">[CS8381]</span></td>
						<td class="code-lap">CS8381</td>
						<td>
							<select id="22"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Object Oriented Programming Laboratory<span class="code">[CS8383]</span></td>
						<td class="code-lap">CS8383</td>
						<td>
							<select id="23"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Digital Systems Laboratory<span class="code">[CS8382]</span></td>
						<td class="code-lap">CS8382</td>
						<td>	
							<select id="24"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Interpersonal Skills/Listening &Speaking<span class="code">[HS8381]</span></td>
						<td class="code-lap">HS8381</td>
						<td>
							<select id="25"  credit="1">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-4]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Probability and Queueing Theory<span class="code">[MA8402]</span></td>
						<td class="code-lap">MA8402</td>
						<td>
							<select id="26"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Computer Architecture<span class="code">[CS8491]</span></td>
						<td class="code-lap">CS8491</td>
						<td>
							<select id="27"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Database Management Systems<span class="code">[CS8492]</span></td>
						<td class="code-lap">CS8492</td>
						<td>	
							<select id="28"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Design and Analysis of Algorithms<span class="code">[CS8451]</span></td>
						<td class="code-lap">CS8451</td>
						<td>	
							<select id="29"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Operating Systems<span class="code">[CS8493]</span></td>
						<td class="code-lap">CS8493</td>
						<td>	
							<select id="30"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Software Engineering<span class="code">[CS8494]</span></td>
						<td class="code-lap">CS8494</td>
						<td>
							<select id="31"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Database Management Systems Laboratory<span class="code">[CS8481]</span></td>
						<td class="code-lap">CS8481</td>
						<td>
							<select id="32"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Operating Systems Laboratory<span class="code">[CS8461]</span></td>
						<td class="code-lap">CS8461</td>
						<td>	
							<select id="33"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Advanced Reading and Writing<span class="code">[HS8461]</span></td>
						<td class="code-lap">HS8461</td>
						<td>
							<select id="34"  credit="1">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
			</div>
			<div class="row">
				<div class="col col-md-6">
					<table class="content-table">
					<tr>
							<th>Subject [Semester-5]</th>
							<th class="code-lap">Code</th>
							<th>Grade</th>
					</tr>	
					<tr>
						<td>Algebra and Number Theory<span class="code">[MA8551]</span></td>
						<td class="code-lap">MA8551</td>
						<td>
							<select id="35"  credit="4">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Computer Networks<span class="code">[CS8591]</span></td>
						<td class="code-lap">CS8591</td>
						<td>
							<select id="36"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Microprocessors and Microcontrollers<span class="code">[EC8691]</span></td>
						<td class="code-lap">EC8691</td>
						<td>	
							<select id="37"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Theory of Computation<span class="code">[CS8501]</span></td>
						<td class="code-lap">CS8501</td>
						<td>	
							<select id="38"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Object Oriented Analysis and Design<span class="code">[CS8592]</span></td>
						<td class="code-lap">CS8592</td>
						<td>	
							<select id="39"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
						
					<tr>
						<td>Open Elective I<span class="code">[---]</span></td>
						<td class="code-lap">---</td>
						<td>
							<select id="40"  credit="3">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Microprocessors and Microcontrollers Laboratory<span class="code">[EC8681]</span></td>
						<td class="code-lap">EC8681</td>
						<td>
							<select id="41"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Object Oriented Analysis and Design Laboratory<span class="code">[CS8582]</span></td>
						<td class="code-lap">CS8582</td>
						<td>	
							<select id="42"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					<tr>
						<td>Networks Laboratory<span class="code">[CS8581]</span></td>
						<td class="code-lap">CS8581</td>
						<td>
							<select id="43"  credit="2">
								<option value="10">O</option>
								<option value="9">A+</option>
								<option value="8">A</option>
								<option value="7">B+</option>
								<option value="6">B</option>
								<option value="0">RA</option>
							</select>
						</td>
					</tr>
						
					</table>
					
				</div>
				<div class="col col-md-6">
					<div class="result group">
						<p>Cumulative Grade Point Average(CGPA):</p>
						<div id="ans"></div>
						<P>Percentage:</P>
						<div class="percentage" id="perc-resp">
							<div class="percentage-amount" id="percentage"></div>
						</div>
						<P><button id="singlejs" class="calc-button calc-size">Calculate</button>
						<P><button id="save" class="calc-button calc-size">Save</button>
					</div>
				</div>
			</div>
			<!--grid used-->

		</div>



	</div>
</div>


			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script><script type="text/javascript" src="../../jquery.js"></script><script type="text/javascript" src="../../theme.js"></script><script src="../thememod.js"></script>

			<script type="text/javascript">
		$(document).ready(function()
		{
			var i;
			var sub_no=1;
			var mark=<?php echo json_encode($mark); ?>;
			$("select").each(function()
			{
				i=this.id;				
				var a1=$('#'+i).val(mark[sub_no]);
				sub_no++;
			});
	  		var reg1=<?php echo $reg; ?>;
	  		$("#save").click(function()
  			{
				sub_no=1;
				$("select").each(function()
				{
					i=this.id;	
					mark[sub_no]=$('#'+i).val();								
					sub_no++;
				});
  				
		  		 $.ajax({
	                type: "POST",
	                url: '../../../Update Files/cse.php',
	                data : {
	                	reg2:reg1,
                     	mark2:mark,           
	                },
	                success: function(data)
	                {
	                 if(data==1)
	                 {

	                    alert("Marks Updated Successful");	                   
	                 }  
	                 else
	                 {
	                    alert("Something is Wrong Please Contact admin");
	                 }                    
	                }
	            });	

	        });

		});				
	</script>
			
	</body>
</html>