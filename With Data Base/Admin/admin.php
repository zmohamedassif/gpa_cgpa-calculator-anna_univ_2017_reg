<?php
session_start();
$servername = "localhost";
$username = "students";
$password = "password";
$dbname = "gpa";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$det=$_SESSION['user_id'];
$dept=array('ECE','CSE','EEE','MECH','CIVIL');
$clge = array('KRCE', 'KRCT', 'MKCE');
$ece=array();
$cse=array();
$eee=array();
$mech=array();
$civil=array();

    if( $_SESSION['admin_name']=='assif')
    {
        for($j=0;$j<5;$j++)
        {
            for($i=0;$i<3;$i++)
            {
                $sql = "SELECT COUNT('reg no') As total FROM details WHERE department = '$dept[$j]' and college = '$clge[$i]' ";

                $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) == 1) {
                    $row = mysqli_fetch_assoc($result);
                    if($dept[$j]=='ECE')
                        $ece[$i]=$row["total"];                 
                    if($dept[$j]=='CSE')
                        $cse[$i]=$row["total"];   
                    if($dept[$j]=='EEE')
                        $eee[$i]=$row["total"]; 
                    if($dept[$j]=='MECH')
                        $mech[$i]=$row["total"]; 
                    if($dept[$j]=='CIVIL')
                        $civil[$i]=$row["total"];  
                }
            }
            $sql = "SELECT COUNT('reg no') As total FROM details WHERE department = '$dept[$j]' and college != 'KRCE' and college != 'KRCT' and college != 'MKCE' ";
            
            $result = mysqli_query($conn, $sql);

            if (mysqli_num_rows($result) == 1) {
                $row = mysqli_fetch_assoc($result);
                if($dept[$j]=='ECE')
                    $ece[3]=$row["total"];                 
                if($dept[$j]=='CSE')
                    $cse[3]=$row["total"];   
                if($dept[$j]=='EEE')
                    $eee[3]=$row["total"]; 
                if($dept[$j]=='MECH')
                    $mech[3]=$row["total"]; 
                if($dept[$j]=='CIVIL')
                    $civil[3]=$row["total"]; 
            } 
        }
            $sql = "SELECT COUNT(*) AS 'registerd' FROM `details` WHERE 1";
            $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) == 1) {
                    $row = mysqli_fetch_assoc($result);                        
                        $total=$row["registerd"]; 
                } 
    }

?>
<html>
	<head>
		<link rel="stylesheet" href="../GPA-CGPA/theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>
	<body> 
		<div class="image" id="cl">

		</div>
		<div class="total-content-background">
			<div class="description" id="cont">

            <h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1>
					<h1 class="heading row" style="margin-right: 0.2%">
						<div class="col col-md-11">Anna University GPA/CGPA Calculator-2017 Regulation</div>
						<div class="col col-md-1">
							<a href="../logout.php" style="background-color: lightyellow;text-decoration:none;padding:5px; color:black">Logout
							</a>
						</div>
					</h1>
		
				<div class="content home-content">					
                    <h1 class="content-heading">Admin Page</h1>

                        <div class="row">
                        
                            <div class="col col-md-6">
                                <table class="content-table">					
                                    <tr>
                                        <th>DEPT</th>
                                        <th>KRCE</th>
                                        <th>KRCT</th>
                                        <th>MKCE</th>
                                        <th>OTHER</th>
                                                            
                                    </tr>
                                        
                                    <tr>
                                        <td>ECE</td>
                                        <td> <?php echo $ece[0];?> </td>
                                        <td> <?php echo $ece[1];?> </td>
                                        <td> <?php echo $ece[2];?> </td>
                                        <td> <?php echo $ece[3];?> </td>
                                    </tr>
                                        
                                    <tr>
                                        <td>CSE</td>
                                        <td> <?php echo $cse[0];?> </td>
                                        <td> <?php echo $cse[1];?> </td>
                                        <td> <?php echo $cse[2];?> </td>
                                        <td> <?php echo $cse[3];?> </td>
                                    </tr>

                                    <tr>
                                        <td>EEE</td>
                                        <td> <?php echo $eee[0];?> </td>
                                        <td> <?php echo $eee[1];?> </td>
                                        <td> <?php echo $eee[2];?> </td>
                                        <td> <?php echo $eee[3];?> </td>
                                    </tr>

                                    <tr>
                                        <td>MECH</td>
                                        <td> <?php echo $mech[0];?> </td>
                                        <td> <?php echo $mech[1];?> </td>
                                        <td> <?php echo $mech[2];?> </td>
                                        <td> <?php echo $mech[3];?> </td>
                                    </tr>

                                    <tr>
                                        <td>CIVIL</td>
                                        <td> <?php echo $civil[0];?> </td>
                                        <td> <?php echo $civil[1];?> </td>
                                        <td> <?php echo $civil[2];?> </td>
                                        <td> <?php echo $civil[3];?> </td>
                                    </tr>                            
                                                                
                                </table> 
                                <a href="../editpswd.php"  class="links" style="margin-left:1%">Chage Admin Password</a><br><br>
                            </div>   

                            <div class="col col-md-6">
                                <table class="content-table">	
                                    <tr class="tablemod">
                                        
                                        <td class="tablemod">
                                            Total Users:<?php echo $total-1 ?><br>
                                            <a href="entiredb.php" class="links">Entire Database</a><br><br>
                                            Register Number:<br>
                                            <input type="text" name="regno" id="regno" class="inputmod"><br>                                                                                            
                                        </td>						                                        
                                    </tr>                                                                                                 
                                                
                                </table>
                                <div>														
                                        <input style="margin-left:2%"class="submitmod login-button" type="submit" id="SUBMIT">				
                                </div>                                                                                              
                            </div>

                        </div>

				</div>
			</div>				
		</div>
	</body>
	<script type="text/javascript" src="admin.js"></script>
</html>
