<?php
session_start();
$servername = "localhost";
$username = "students";
$password = "password";
$dbname = "gpa";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


if( $_SESSION['admin_name']=='assif')
{
	$sql = " SELECT * FROM `details` ";
}
$result = mysqli_query($conn, $sql);

?>
<html>
	<head>
		<link rel="stylesheet" href="../GPA-CGPA/theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>
	<body> 
		<div class="image" id="cl">

		</div>
		<div class="total-content-background">
			<div class="description" id="cont">

					<h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1>
					<h1 class="heading row" style="margin-right: 0.2%">
						<div class="col col-md-11">Anna University GPA/CGPA Calculator-2017 Regulation</div>
						<div class="col col-md-1">
							<a href="../logout.php" style="background-color: lightyellow;text-decoration:none;padding:5px; color:black">Logout
							</a>
						</div>
					</h1>

					<div class="content home-content">
						
						<div>							
							<h1 class="content-heading">Admin Login</h1>
							<table class="content-table" style="font-size:11px">					
								<tr>
                                    <th>Name</th>
                                    <th>Register No</th>
                                    <th>Department</th>
                                    <th>Email</th>
                                    <th>College</th>
                                    <th>Password</th>
                                </tr>
                                <?php
                                    
                                        if (mysqli_num_rows($result) >0 ) {
                                            while($row = mysqli_fetch_assoc($result)) {
                                                echo '<tr>';
                                                echo "<td> " . $row["name"]." </td>" ;
                                                echo "<td> " . $row["reg no"]." </td>" ;
                                                echo "<td> " . $row["department"]." </td>" ;
                                                echo "<td> " . $row["email"]." </td>" ;
                                                echo "<td> " . $row["college"]." </td>" ;
                                                echo "<td> " . $row["password"]." </td>" ;
                                                echo "</tr>";
                                            }
                                        }
                                    
								?>                                                                                   
                            </table> 												                                                                                                                                             
						</div>
					</div>
			</div>
		</div>	
	</body>
</html>