<?php
session_start();
$servername = "localhost";
$username = "students";
$password = "password";
$dbname = "gpa";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$det=$_SESSION['user_id'];

$sql = "SELECT * FROM `details` WHERE `reg no` LIKE '$det' ";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) == 1) {
   $row = mysqli_fetch_assoc($result);
        $a=$row["name"];
        $b=$row["reg no"];
        $c=$row["department"];
        $d=$row["email"];
        $e=$row["college"];
    }
?>
<html>
	<head>
		<link rel="stylesheet" href="GPA-CGPA/theme.css">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>
	<body> 
        <div class="image" id="cl">

        </div>
        <div class="total-content-background">
            <div class="description" id="cont">

                    <h1 class="heading" style="text-align:center">K Ramakrishnan Group of Institutions</h1>
                    <h1 class="heading"  style="text-align:center">
                        <div>Anna University GPA/CGPA Calculator-2017 Regulation</div>
                    </h1>
            
                    <div class="content home-content">					
                        <h1 class="content-heading"  style="text-align:center">Edit Profile</h1>

                        <table class="content-table table-acc">	
                            <tr class="tablemod">
                                <td class="tablemod">Register Number:</td>                                
                                <td class="tablemod">
                                    <?php echo $b ?>
                                </td>
                            </tr>
                            <tr class="tablemod">
                                <td class="tablemod">Department:</td>                                
                                <td class="tablemod">	
                                    <?php echo $c ?>
                                </td>
                            </tr>
                            <tr class="tablemod">
                                <td class="tablemod">Name:</td>
                                <td class="tablemod"><input class="inputmod" type="text" id="name" ></td>						
                            </tr>						                                                    
                            <tr>
                                <td class="tablemod">E-mail:</td>
                                <td class="tablemod">
                                    <input class="inputmod" type="email" id="email" >
                                    <span id="error3" style="color:red;font-size: 20px;font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablemod">College:</td>
                                <td class="tablemod">
                                    <select id="college" class="selectmod">
                                        <option value="KRCE">K.Ramakrishnan College of Engineering</option>
                                        <option value="KRCT">K.Ramakrishnan College of Technology</option>
                                        <option value="MKCE">M.Kumarasamy College of Engineering</option>
                                        <option value="other">other</option>
                                    </select>
                                    <span id="othername" style="display: none"><input class="inputmod" type="text" id="other" placeholder="Enter Your College Name"></span>
                                </td>						
                            </tr>                            						                            				
                        </table> 
                        <div class="submitalign">                            									
                            <button class="submitmod login-button" type="submit" id="doit" style="background-color:black;color:white">Update</button>
                        </div>                       			                                    
                    </div>
            </div>
        </div>

    </body>
    <script type="text/javascript" src="edit.js"></script>
    <script>
        $(document).ready(function()
		{
            $('#name').val("<?php echo $a; ?>");
            $('#email').val("<?php echo $d; ?>");
            if( "<?php echo $e ?>" != "KRCE" && "<?php echo $e ?>" != "KRCT" && "<?php echo $e ?>" != "MKCE" )
            {
                $('#college').val("other");
                document.getElementById("othername").style.display="block";
                $('#other').val("<?php echo $e ?>");
            }
            else
            {
                $('#college').val("<?php echo $e ?>");
            }  
        });
    </script>    
</html>