About:

    This project was developed to calculate the Grade Point Average(GPA) and Cumulative Grade Point Average(CGPA) for Anna University 2017 Regulation and save their academic results.

Database Creation:

    Import the "gpa-cgpa-database-query.sql" file which will create the database "gpa" which has the following structure, create user account "students" and grand all privileges to user on database.
    
DataBase Structure:
    
    DataBase Name: gpa
    
    Table 1:(Student Profile)
        
        Table Name: details
        
        Table Structure:
        ________________________________________________________________
        |	name  |  reg no | department | email |  college | password |
        ----------------------------------------------------------------
        |         |         |            |       |          |          |
        |_________|_________|____________|_______|__________|__________|
        
    Table 2:(ECE Marks)
    
        Table Name: ece
        
        Table Structure:
        ______________________________..........______
        |   register no  |  1| 2 | 3 |          |62  |
        ----------------------------------------------
        |                |   |   |   |          |    |          
        |________________|___|___|___|..........|____|
        
    Table 3:(CSE Marks)
    
        Table Name: cse
        
        Table Structure:
        ______________________________..........______
        |   reg no       |  1| 2 | 3 |          |64  |
        ----------------------------------------------
        |                |   |   |   |          |    |          
        |________________|___|___|___|..........|____|
    
    Table 4:(EEE Marks)
    
        Table Name: eee
        
        Table Structure:
        ______________________________..........______
        |   reg no       |  1| 2 | 3 |          |61  |
        ----------------------------------------------
        |                |   |   |   |          |    |          
        |________________|___|___|___|..........|____|
        
    Table 5:(MECH Marks)
    
        Table Name: mech
        
        Table Structure:
        ______________________________..........______
        |   reg no       |  1| 2 | 3 |          |63  |
        ----------------------------------------------
        |                |   |   |   |          |    |          
        |________________|___|___|___|..........|____|
        
    Table 5:(CIVIL Marks)
    
        Table Name: civil
        
        Table Structure:
        ______________________________..........______
        |   reg no       |  1| 2 | 3 |          |62  |
        ----------------------------------------------
        |                |   |   |   |          |    |          
        |________________|___|___|___|..........|____|
        
Admin login:

    Register Number:admin
    Password       :1981214113