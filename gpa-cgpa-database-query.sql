-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 07, 2019 at 02:17 PM
-- Server version: 5.7.24-log
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gpa`
--
CREATE DATABASE IF NOT EXISTS `gpa` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `gpa`;

-- --------------------------------------------------------

--
-- Table structure for table `civil`
--

CREATE TABLE `civil` (
  `reg no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `1` varchar(100) NOT NULL,
  `2` varchar(100) NOT NULL,
  `3` varchar(100) NOT NULL,
  `4` varchar(100) NOT NULL,
  `5` varchar(100) NOT NULL,
  `6` varchar(100) NOT NULL,
  `7` varchar(100) NOT NULL,
  `8` varchar(100) NOT NULL,
  `9` varchar(100) NOT NULL,
  `10` varchar(100) NOT NULL,
  `11` varchar(100) NOT NULL,
  `12` varchar(100) NOT NULL,
  `13` varchar(100) NOT NULL,
  `14` varchar(100) NOT NULL,
  `15` varchar(100) NOT NULL,
  `16` varchar(100) NOT NULL,
  `17` varchar(100) NOT NULL,
  `18` varchar(100) NOT NULL,
  `19` varchar(100) NOT NULL,
  `20` varchar(100) NOT NULL,
  `21` varchar(100) NOT NULL,
  `22` varchar(100) NOT NULL,
  `23` varchar(100) NOT NULL,
  `24` varchar(100) NOT NULL,
  `25` varchar(100) NOT NULL,
  `26` varchar(100) NOT NULL,
  `27` varchar(100) NOT NULL,
  `28` varchar(100) NOT NULL,
  `29` varchar(100) NOT NULL,
  `30` varchar(100) NOT NULL,
  `31` varchar(100) NOT NULL,
  `32` varchar(100) NOT NULL,
  `33` varchar(100) NOT NULL,
  `34` varchar(100) NOT NULL,
  `35` varchar(100) NOT NULL,
  `36` varchar(100) NOT NULL,
  `37` varchar(100) NOT NULL,
  `38` varchar(100) NOT NULL,
  `39` varchar(100) NOT NULL,
  `40` varchar(100) NOT NULL,
  `41` varchar(100) NOT NULL,
  `42` varchar(100) NOT NULL,
  `43` varchar(100) NOT NULL,
  `44` varchar(100) NOT NULL,
  `45` varchar(100) NOT NULL,
  `46` varchar(100) NOT NULL,
  `47` varchar(100) NOT NULL,
  `48` varchar(100) NOT NULL,
  `49` varchar(100) NOT NULL,
  `50` varchar(100) NOT NULL,
  `51` varchar(100) NOT NULL,
  `52` varchar(100) NOT NULL,
  `53` varchar(100) NOT NULL,
  `54` varchar(100) NOT NULL,
  `55` varchar(100) NOT NULL,
  `56` varchar(100) NOT NULL,
  `57` varchar(100) NOT NULL,
  `58` varchar(100) NOT NULL,
  `59` varchar(100) NOT NULL,
  `60` varchar(100) NOT NULL,
  `61` varchar(100) NOT NULL,
  `62` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cse`
--

CREATE TABLE `cse` (
  `reg no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `1` varchar(100) NOT NULL,
  `2` varchar(100) NOT NULL,
  `3` varchar(100) NOT NULL,
  `4` varchar(100) NOT NULL,
  `5` varchar(100) NOT NULL,
  `6` varchar(100) NOT NULL,
  `7` varchar(100) NOT NULL,
  `8` varchar(100) NOT NULL,
  `9` varchar(100) NOT NULL,
  `10` varchar(100) NOT NULL,
  `11` varchar(100) NOT NULL,
  `12` varchar(100) NOT NULL,
  `13` varchar(100) NOT NULL,
  `14` varchar(100) NOT NULL,
  `15` varchar(100) NOT NULL,
  `16` varchar(100) NOT NULL,
  `17` varchar(100) NOT NULL,
  `18` varchar(100) NOT NULL,
  `19` varchar(100) NOT NULL,
  `20` varchar(100) NOT NULL,
  `21` varchar(100) NOT NULL,
  `22` varchar(100) NOT NULL,
  `23` varchar(100) NOT NULL,
  `24` varchar(100) NOT NULL,
  `25` varchar(100) NOT NULL,
  `26` varchar(100) NOT NULL,
  `27` varchar(100) NOT NULL,
  `28` varchar(100) NOT NULL,
  `29` varchar(100) NOT NULL,
  `30` varchar(100) NOT NULL,
  `31` varchar(100) NOT NULL,
  `32` varchar(100) NOT NULL,
  `33` varchar(100) NOT NULL,
  `34` varchar(100) NOT NULL,
  `35` varchar(100) NOT NULL,
  `36` varchar(100) NOT NULL,
  `37` varchar(100) NOT NULL,
  `38` varchar(100) NOT NULL,
  `39` varchar(100) NOT NULL,
  `40` varchar(100) NOT NULL,
  `41` varchar(100) NOT NULL,
  `42` varchar(100) NOT NULL,
  `43` varchar(100) NOT NULL,
  `44` varchar(100) NOT NULL,
  `45` varchar(100) NOT NULL,
  `46` varchar(100) NOT NULL,
  `47` varchar(100) NOT NULL,
  `48` varchar(100) NOT NULL,
  `49` varchar(100) NOT NULL,
  `50` varchar(100) NOT NULL,
  `51` varchar(100) NOT NULL,
  `52` varchar(100) NOT NULL,
  `53` varchar(100) NOT NULL,
  `54` varchar(100) NOT NULL,
  `55` varchar(100) NOT NULL,
  `56` varchar(100) NOT NULL,
  `57` varchar(100) NOT NULL,
  `58` varchar(100) NOT NULL,
  `59` varchar(100) NOT NULL,
  `60` varchar(100) NOT NULL,
  `61` varchar(100) NOT NULL,
  `62` varchar(100) NOT NULL,
  `63` varchar(100) NOT NULL,
  `64` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `name` varchar(250) NOT NULL,
  `reg no` varchar(250) NOT NULL,
  `department` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `college` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`name`, `reg no`, `department`, `email`, `college`, `password`) VALUES
('', 'admin', '', '', '', '1981214113');

-- --------------------------------------------------------

--
-- Table structure for table `ece`
--

CREATE TABLE `ece` (
  `register no` varchar(100) NOT NULL,
  `1` varchar(100) NOT NULL,
  `2` varchar(100) NOT NULL,
  `3` varchar(100) NOT NULL,
  `4` varchar(100) NOT NULL,
  `5` varchar(100) NOT NULL,
  `6` varchar(100) NOT NULL,
  `7` varchar(100) NOT NULL,
  `8` varchar(100) NOT NULL,
  `9` varchar(100) NOT NULL,
  `10` varchar(100) NOT NULL,
  `11` varchar(100) NOT NULL,
  `12` varchar(100) NOT NULL,
  `13` varchar(100) NOT NULL,
  `14` varchar(100) NOT NULL,
  `15` varchar(100) NOT NULL,
  `16` varchar(100) NOT NULL,
  `17` varchar(100) NOT NULL,
  `18` varchar(100) NOT NULL,
  `19` varchar(100) NOT NULL,
  `20` varchar(100) NOT NULL,
  `21` varchar(100) NOT NULL,
  `22` varchar(100) NOT NULL,
  `23` varchar(100) NOT NULL,
  `24` varchar(100) NOT NULL,
  `25` varchar(100) NOT NULL,
  `26` varchar(100) NOT NULL,
  `27` varchar(100) NOT NULL,
  `28` varchar(100) NOT NULL,
  `29` varchar(100) NOT NULL,
  `30` varchar(100) NOT NULL,
  `31` varchar(100) NOT NULL,
  `32` varchar(100) NOT NULL,
  `33` varchar(100) NOT NULL,
  `34` varchar(100) NOT NULL,
  `35` varchar(100) NOT NULL,
  `36` varchar(100) NOT NULL,
  `37` varchar(100) NOT NULL,
  `38` varchar(100) NOT NULL,
  `39` varchar(100) NOT NULL,
  `40` varchar(100) NOT NULL,
  `41` varchar(100) NOT NULL,
  `42` varchar(100) NOT NULL,
  `43` varchar(100) NOT NULL,
  `44` varchar(100) NOT NULL,
  `45` varchar(100) NOT NULL,
  `46` varchar(100) NOT NULL,
  `47` varchar(100) NOT NULL,
  `48` varchar(100) NOT NULL,
  `49` varchar(100) NOT NULL,
  `50` varchar(100) NOT NULL,
  `51` varchar(100) NOT NULL,
  `52` varchar(100) NOT NULL,
  `53` varchar(100) NOT NULL,
  `54` varchar(100) NOT NULL,
  `55` varchar(100) NOT NULL,
  `56` varchar(100) NOT NULL,
  `57` varchar(100) NOT NULL,
  `58` varchar(100) NOT NULL,
  `59` varchar(100) NOT NULL,
  `60` varchar(100) NOT NULL,
  `61` varchar(100) NOT NULL,
  `62` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `eee`
--

CREATE TABLE `eee` (
  `reg no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `1` varchar(100) NOT NULL,
  `2` varchar(100) NOT NULL,
  `3` varchar(100) NOT NULL,
  `4` varchar(100) NOT NULL,
  `5` varchar(100) NOT NULL,
  `6` varchar(100) NOT NULL,
  `7` varchar(100) NOT NULL,
  `8` varchar(100) NOT NULL,
  `9` varchar(100) NOT NULL,
  `10` varchar(100) NOT NULL,
  `11` varchar(100) NOT NULL,
  `12` varchar(100) NOT NULL,
  `13` varchar(100) NOT NULL,
  `14` varchar(100) NOT NULL,
  `15` varchar(100) NOT NULL,
  `16` varchar(100) NOT NULL,
  `17` varchar(100) NOT NULL,
  `18` varchar(100) NOT NULL,
  `19` varchar(100) NOT NULL,
  `20` varchar(100) NOT NULL,
  `21` varchar(100) NOT NULL,
  `22` varchar(100) NOT NULL,
  `23` varchar(100) NOT NULL,
  `24` varchar(100) NOT NULL,
  `25` varchar(100) NOT NULL,
  `26` varchar(100) NOT NULL,
  `27` varchar(100) NOT NULL,
  `28` varchar(100) NOT NULL,
  `29` varchar(100) NOT NULL,
  `30` varchar(100) NOT NULL,
  `31` varchar(100) NOT NULL,
  `32` varchar(100) NOT NULL,
  `33` varchar(100) NOT NULL,
  `34` varchar(100) NOT NULL,
  `35` varchar(100) NOT NULL,
  `36` varchar(100) NOT NULL,
  `37` varchar(100) NOT NULL,
  `38` varchar(100) NOT NULL,
  `39` varchar(100) NOT NULL,
  `40` varchar(100) NOT NULL,
  `41` varchar(100) NOT NULL,
  `42` varchar(100) NOT NULL,
  `43` varchar(100) NOT NULL,
  `44` varchar(100) NOT NULL,
  `45` varchar(100) NOT NULL,
  `46` varchar(100) NOT NULL,
  `47` varchar(100) NOT NULL,
  `48` varchar(100) NOT NULL,
  `49` varchar(100) NOT NULL,
  `50` varchar(100) NOT NULL,
  `51` varchar(100) NOT NULL,
  `52` varchar(100) NOT NULL,
  `53` varchar(100) NOT NULL,
  `54` varchar(100) NOT NULL,
  `55` varchar(100) NOT NULL,
  `56` varchar(100) NOT NULL,
  `57` varchar(100) NOT NULL,
  `58` varchar(100) NOT NULL,
  `59` varchar(100) NOT NULL,
  `60` varchar(100) NOT NULL,
  `61` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mech`
--

CREATE TABLE `mech` (
  `reg no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `1` varchar(100) NOT NULL,
  `2` varchar(100) NOT NULL,
  `3` varchar(100) NOT NULL,
  `4` varchar(100) NOT NULL,
  `5` varchar(100) NOT NULL,
  `6` varchar(100) NOT NULL,
  `7` varchar(100) NOT NULL,
  `8` varchar(100) NOT NULL,
  `9` varchar(100) NOT NULL,
  `10` varchar(100) NOT NULL,
  `11` varchar(100) NOT NULL,
  `12` varchar(100) NOT NULL,
  `13` varchar(100) NOT NULL,
  `14` varchar(100) NOT NULL,
  `15` varchar(100) NOT NULL,
  `16` varchar(100) NOT NULL,
  `17` varchar(100) NOT NULL,
  `18` varchar(100) NOT NULL,
  `19` varchar(100) NOT NULL,
  `20` varchar(100) NOT NULL,
  `21` varchar(100) NOT NULL,
  `22` varchar(100) NOT NULL,
  `23` varchar(100) NOT NULL,
  `24` varchar(100) NOT NULL,
  `25` varchar(100) NOT NULL,
  `26` varchar(100) NOT NULL,
  `27` varchar(100) NOT NULL,
  `28` varchar(100) NOT NULL,
  `29` varchar(100) NOT NULL,
  `30` varchar(100) NOT NULL,
  `31` varchar(100) NOT NULL,
  `32` varchar(100) NOT NULL,
  `33` varchar(100) NOT NULL,
  `34` varchar(100) NOT NULL,
  `35` varchar(100) NOT NULL,
  `36` varchar(100) NOT NULL,
  `37` varchar(100) NOT NULL,
  `38` varchar(100) NOT NULL,
  `39` varchar(100) NOT NULL,
  `40` varchar(100) NOT NULL,
  `41` varchar(100) NOT NULL,
  `42` varchar(100) NOT NULL,
  `43` varchar(100) NOT NULL,
  `44` varchar(100) NOT NULL,
  `45` varchar(100) NOT NULL,
  `46` varchar(100) NOT NULL,
  `47` varchar(100) NOT NULL,
  `48` varchar(100) NOT NULL,
  `49` varchar(100) NOT NULL,
  `50` varchar(100) NOT NULL,
  `51` varchar(100) NOT NULL,
  `52` varchar(100) NOT NULL,
  `53` varchar(100) NOT NULL,
  `54` varchar(100) NOT NULL,
  `55` varchar(100) NOT NULL,
  `56` varchar(100) NOT NULL,
  `57` varchar(100) NOT NULL,
  `58` varchar(100) NOT NULL,
  `59` varchar(100) NOT NULL,
  `60` varchar(100) NOT NULL,
  `61` varchar(100) NOT NULL,
  `62` varchar(100) NOT NULL,
  `63` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `civil`
--
ALTER TABLE `civil`
  ADD PRIMARY KEY (`reg no`);

--
-- Indexes for table `cse`
--
ALTER TABLE `cse`
  ADD PRIMARY KEY (`reg no`);

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`reg no`);

--
-- Indexes for table `ece`
--
ALTER TABLE `ece`
  ADD PRIMARY KEY (`register no`);

--
-- Indexes for table `eee`
--
ALTER TABLE `eee`
  ADD PRIMARY KEY (`reg no`);

--
-- Indexes for table `mech`
--
ALTER TABLE `mech`
  ADD PRIMARY KEY (`reg no`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE USER 'students'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON gpa . * TO 'students'@'localhost';

FLUSH PRIVILEGES;
